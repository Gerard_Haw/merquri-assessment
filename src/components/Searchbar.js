import React,{useState} from 'react'
import './Searchbar.css'

function Searchbar({data}){

    const [filteredData,setFilteredData] = useState([]);

    const handleFilter = (event)=>{
        const word = event.target.value;
        const newFilter = data.filter((value)=>{
            return value.title.toLowerCase().includes(word.toLowerCase());
        });

        setFilteredData(newFilter);
    }

    return (
        <div className='search'>
            <div className="searchInputs">
                <input type="text" placeholder='Search' onChange={handleFilter} />
            </div>
            {filteredData.length !== 0 && (
            <div className='dataResult'>
                {filteredData.map((value)=>{
                    return <div className='data-item'> {value.title} </div>
                })}
            </div>
            )}
        </div>
    )
}


export default Searchbar;