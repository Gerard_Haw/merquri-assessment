import React, { useState } from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { Link } from 'react-router-dom';
import './Navbar.css';
import { IconContext } from 'react-icons';
import Searchbar from './Searchbar';

function Navbar() {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  const SampleData = [{title:"Captain America"},{title:"Iron Man"},{title:"Spiderman"},{title:"Hawkeye"},{title:"Hulk"}];

  return (
    <>
      <IconContext.Provider value={{ color: '#fff' }}>
        <div className='navbar'>
          <Link to='#' className='menu-bars'>
            <FaIcons.FaBars onClick={showSidebar} />
          </Link>
        </div>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
          <ul className='nav-menu-items' onClick={showSidebar}>
            <li className='navbar-toggle'>
              <Link to='#' className='menu-bars'>
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            <li className="navbar-item">
                Menu Item 1
            </li>
            <li className="navbar-item">
                Menu Item 2
            </li>
            <li className="navbar-item">
                Menu Item 3
            </li>
          </ul>

                    
        </nav>

        <Searchbar placeholder="Search" data={SampleData} />  
      </IconContext.Provider>
    </>
  );
}

export default Navbar;